# A (very slow) implementation of Monte Carlo Tree Search for Quantum TicTacToe.
#
# Some notes:
#  - Tree is the class for nodes in the game tree
#  - A 'path' is a list of references to nodes in the game tree, where the first one is always
#    understood as the root of the tree.
#  - At each node in the game tree we don't store the state of the board at each node but instead
#    the game movement leading to the node. Instead of pre/finmarks, we store "movecodes".

from qttt import *
import random
from time import time
from math import sqrt, log
from tools import total_size, cdisplay

# Upper bound on the depth of the game tree
MAXMOVES = 20  # TODO: compute this number, place it somewhere


def game_ends_here(t):
    """ A game ends with a collapse and no subsequent move.
     Return true if the node @t is reached through such a movecode. """
    return isinstance(t, Tree) and t.move[0] and not t.move[1]


class Tree:
    """ A tree to store game states.

    Instead of saving the whole board we save the difference wrt. the previous state, i.e.
    the move which led to the current board. """
    def __init__(self, move=None):
        self.ch = []      # Children (list of Tree)
        self.move = move  # Move which transforms the parent node into this node
        self.score = 0
        self.plays = 0
        self.wins = 0     # Actually unnecessary for MCTS, but useful for stats


def find_child(t, move):
    """ Given a tree @t, return the child which is attained by performing @move or None. """
    ch = filter(lambda c: c.move == move, t.ch)
    if len(ch) == 1:
        return ch[0]
    elif len(ch) > 1:
        raise Exception('Several children with same move')
    else:
        return None


def detach(p):
    """ Detaches the last node in the path @p and deletes the root of the tree
    (first node in the path)
    :param p: Path (list of nodes)
    :return: last node in the path
    """
    try:
        t = p[-1]
        p[-2].ch.remove(t)
        del p[0]
        return t
    except IndexError:
        print 'Duh!'


def expand(t, lomc):
    """ Add a new child to tree @t using the list of possible move codes @lomc.
    By default we select one of the move codes at random and use it to generate a new node.
    :param t: the node to which we must append
    :param lomc: list of allowed move codes at the given node
    :return: The newly created node """
    moves = list(lomc)
    while moves:
        mc = random.choice(moves)
        if find_child(t, mc) is None:
            t2 = Tree(move=mc)
            t.ch.append(t2)
            return t2
        moves.remove(mc)
    return t  # FIXME: should we throw here?


def sort_children_by_uct(p, factor=1.0):
    """ Sort the children of the last node in @p in descending order (highest UCT first)
    and return the sorted list. """
    root = p[0]
    node = p[-1]
    ch = node.ch
    if not ch:
        return ch

    def uct(t):
        return t.score / float((1+t.plays) * MAXMOVES) +\
               factor * sqrt(2.0 * log(root.plays) / (1+t.plays))

    ch.sort(key=uct, reverse=True)
    return ch


def tree_policy(p, board, uct_factor=1.0):
    """ Starting at path @p in the game tree, either expand the last node in @p if it is expandable
    or select its most promising child.

    :type p: list
    :type board: Board
    :param p: path of nodes [root, child, child-of-child, ..., node-to-consider]
              This is *modified* by calls to tree_policy().
    :param board: Board object with the state of the board at the tail of the path.
                  This is *modified* by calls to tree_policy().
    :return: True if the tree has been expanded, False if it is already full
    """
    t = p[-1]

    if game_ends_here(t):
        return False

    lomc = board.getListOfMovecodes()

    if len(lomc) is 0:  # FIXME: when can this happen? (Only after the board is full)
        # print("\n\n******************************************\n\n")
        # print(board)
        # for n in p:
        #     print("n.move={}".format(n.move))
        # import sys
        # sys.stdout.flush()
        # raise Exception("WTF?")
        return False
    elif len(lomc) > len(t.ch):  # There are movements left to be made from this node
        # This is not strictly part of the tree policy, but doing it here saves recomputing or
        # storing the lomc in a subsequent call to expand()
        t2 = expand(t, lomc)
        p.append(t2)
        return True
    else:
        bc = board.copy()
        pc = list(p)
        ch = sort_children_by_uct(p, uct_factor)
        for n in ch:
            p.append(n)
            board.makeMove(n.move)
            if tree_policy(p, board):
                return True
            board = bc.copy()
            p = list(pc)

        return False


def simulate(p, board=None):
    """ Starting at path @p in the game tree, simulate further moves until the game ends.

    :type p: list
    :type board: Board
    :param p: path of nodes [root, child, child-of-child, ..., node-to-consider]
    :param board: Board object with the state of the board at the tail of the path. This is used
                  internally during recursion to avoid too many recomputations of the board.
                  This is *modified* by calls to simulate()
    """
    t = p[-1]
    if game_ends_here(t):
        m = t.move[0]
        board.collapse(m[0], m[1], m[2], m[3])
        return

    lomc = board.getListOfMovecodes()

    if lomc:
        t2 = expand(t, lomc)
        p.append(t2)
        board.makeMove(t2.move)
        simulate(p, board)


def backpropagate(p, board):
    previous_letter = p[0].move[1][0]
    # assert previous_letter in 'XO'
    factor = MAXMOVES - len(p)  # Favor longer paths if we lose or shorter if we win
    factor = max(1, factor)  # TODO: remove this once we compute the right value for MAXMOVES
    pts = board.value(other_letter(previous_letter), previous_letter) * factor
    for n in p:
        n.plays += 1
        n.score += pts
        n.wins += 1 if pts > 0 else 0
    return


def best_child(t):
    # TODO: implement alternative policies
    if not t.ch:
        raise Exception("Huh?")
    l = [t.ch[0]]
    for n in t.ch[1:]:
        if n.score > l[0].score:
            l = [n]
        elif n.score == l[0].score:
            l.append(n)
    return random.choice(l)


def mcts(board, stored_tree, time_available=2.0, uct_factor=1.0, debug_win=None):
    """ Driver of the Monte Carlo Tree Search
    :param board:
    :param time_available: in seconds, floating point
    :return: best child
    """
    mc = board.lastMovecode()
    root = (find_child(stored_tree, mc) or Tree(move=mc))
    start = time()
    time_elapsed = 0.0
    maxpath = 0
    while time_available > time_elapsed:
        b = board.copy()
        p = [root]
        if not tree_policy(p, b, uct_factor):  # The game tree is complete
            break
        simulate(p, b)
        backpropagate(p, b)
        time_elapsed = time() - start
        maxpath = max(len(p), maxpath)

    best = best_child(root)
    # plays, wins = reduce(lambda acc, n: [acc[0]+n.plays, acc[1]+n.wins], root.ch, [0, 0])
    plays = sum(n.plays for n in root.ch)

    def tree_handler(t):
        yield t.__dict__  # Is this ok? Or should I rather yield each member separately?

    cdisplay(debug_win, "MCTS played {0} games with a maximal depth of {1}.\n"
                        "It took it {2:.3} seconds and {5} Kb.\n"
                        "The winning movement has score {3} out of {4} plays "
                        "(wins with probability {6:.2}).".
             format(plays, maxpath, time_elapsed, best.score, best.plays,
                    total_size(root, {Tree: tree_handler})/1024, best.wins/float(best.plays)))

    # print([n.move for n in root.ch])
    # print([n.score for n in root.ch])
    # print([n.plays for n in root.ch])

    return best
