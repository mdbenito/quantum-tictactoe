from tools import available_cpu_count
from concurrent.futures import ProcessPoolExecutor
from play import new_game
from players import MonteCarloPlayer
import numpy as np
import pickle


class Experiment:
    games_per_match = 0
    uct_values = np.array([])
    wins_p1 = np.array([])
    wins_p2 = np.array([])
    times = [0.0, 0.0]
    turn = None
    stats = []  # TODO


def showdown(worker_idx, uct_values, ngames, times, turn):
    players = [MonteCarloPlayer(letter="X", name="Player1",
                                opts={'time_available': times[0], 'uct_factor': uct_values[0]}),
               MonteCarloPlayer(letter="O", name="Player2",
                                opts={'time_available': times[1], 'uct_factor': uct_values[1]})]
    wins = [0, 0]
    for game in range(ngames):
        print("Round {0} of {1} for uct factors ({2}, {3})".
              format(game + 1, ngames, uct_values[0], uct_values[1]))
        winner = new_game(players, turn=turn)
        if winner is not None:
            wins[winner] += 1
    return [worker_idx, uct_values, wins]


def test_uct_params(start, end, step, games_per_match, times, turn):
    exp = Experiment()
    exp.games_per_match = games_per_match
    exp.uct_values = np.arange(start, end + step/2., step)  # HACK to get a closed interval
    n = len(exp.uct_values)
    xx, yy = np.meshgrid(exp.uct_values, exp.uct_values)
    ind = np.triu_indices(n)
    params = np.stack((xx[ind], yy[ind]), 1)
    m_ind = np.stack((ind[0], ind[1]), 1)
    num_jobs = m_ind.shape[0]
    m_games = [games_per_match] * num_jobs
    m_times = [times] * num_jobs
    m_turns = [turn] * num_jobs
    exp.wins_p1 = np.zeros((n, n), dtype=np.int8)
    exp.wins_p2 = np.zeros((n, n), dtype=np.int8)
    exp.times = times
    exp.turn = turn
    processes = available_cpu_count()
    with ProcessPoolExecutor(max_workers=processes) as executor:
        print("Starting {0} matches using {1} processes (num. games={2})".
              format(n, processes, games_per_match))
        for r in executor.map(showdown, m_ind, params, m_games, m_times, m_turns):
            index, uct_values, wins = r
            exp.wins_p1[tuple(index)] = wins[0]
            exp.wins_p2[tuple(index)] = wins[1]
            print("Saving results of job {0}: Factors = {1}, wins = {2}".
                  format(index, uct_values, wins))
            with open("results.pickle", "wb") as out_file:
                pickle.dump(exp, out_file)

if __name__ == "__main__":
    test_uct_params(start=0.0, end=2.0, step=0.1, games_per_match=50, times=[3, 3], turn='random')
    # test_uct_params(start=1.0, end=1.2, step=0.1, games_per_match=5, times=[2.5, 2.5])
