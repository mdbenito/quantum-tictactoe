def minimax(board, steps, totalNumSteps, maximizingPlayer, playerLetter,
            currentNum, adversaryLetter, savedMoves=[], debug=False):
    lomc = board.getListOfMovecodes()
    if steps == 0 or lomc == []:
        if maximizingPlayer:
            return board.value(playerLetter, adversaryLetter), savedMoves
        else:
            return board.value(adversaryLetter, playerLetter), savedMoves

    if maximizingPlayer:
        # print("Is max player")
        maxVal = -100  # should be inf
        for move in lomc:
            # print('Trying move {0}'.format(lomc.getIndex(move)))
            copy = board.copy()
            copy.makeMove(move)
            # print(copy)
            if not move[1]:  # game has ended
                val = copy.value(playerLetter, adversaryLetter)

            else:
                val = minimax(copy, steps - 1, totalNumSteps, False, adversaryLetter,
                              currentNum + 1, playerLetter, savedMoves=savedMoves)[0]
            if val >= maxVal:
                maxVal = val
                if steps == totalNumSteps:
                    savedMoves.append(move)
        # print('Optimal Choice is move {0} with value {1}'.format(savedMoves, maxVal))
        return maxVal, savedMoves
    else:
        minVal = 100
        for move in lomc:
            # print('Trying move {0}'.format(lomc.getIndex(move)))
            copy = board.copy()
            copy.makeMove(move)
            # print(copy)

            if not move[1]:  # game has ended
                return copy.value(adversaryLetter, playerLetter), savedMoves
            else:
                val = minimax(copy, steps - 1, totalNumSteps, True, adversaryLetter,
                              currentNum + 1, playerLetter, savedMoves=savedMoves)[0]
            minVal = min(val, minVal)
        return minVal, savedMoves
