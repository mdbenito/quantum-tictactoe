# Preliminary Marks (e.g. the two instances of "cross1" are called premarks, final marks
# (obtained after collapse) are called finmarks.


class Board:
    def __init__(self, startingLetter):
        """ Initialize Board with list of empty fields. """
        self.fields = [Field(n) for n in range(10)]  # ignore index 0
        self.startingLetter = startingLetter
        # Keeps track of last PreMark on the board.
        self.lastMark = PreMark(other_letter(startingLetter), 0, 0, 0)  # Fake move needed for MCTS

    def copy(self):
        """ Make a (deep) copy of ourselves.
         TODO: maybe reimplement __copy__ and __deepcopy__ """
        b = Board(startingLetter=self.startingLetter)
        b.fields = [f.copy() for f in self.fields]
        b.lastMark = self.lastMark.copy() if self.lastMark else None
        return b

    def addPreMark(self, letter, num, pos1, pos2):
        """ Add a pre-mark with letter in {X,O} with its specific number at positions 1 and 2 """

        self.fields[pos1].addPreMark(PreMark(letter, num, pos1, pos2))
        self.lastMark = PreMark(letter, num, pos2, pos1)
        self.fields[pos2].addPreMark(self.lastMark)
        return self.lastMark

    def addFinMark(self, letter, num, pos):
        """ Add a final mark with letter in {X, O} at position pos """
        self.fields[pos].addFinMark(FinMark(letter, num, pos))

    def lastMovecode(self):
        mark = self.lastMark
        return [[], [mark.letter, mark.num, mark.pos, mark.otherpos]] if mark else [[], []]

    def has_won(self, letter):
        """ Check whether the letter (in {X, O}) has won the game.
        Returns whether @letter has won, together with the lowest max subscript. """
        bo, nb = self.realBoard()
        ttt = [False] * 8
        maxind = [0] * 8
        has_ttt = False
        lowest_max_subscript = -1
        lines = ((7, 8, 9), (4, 5, 6), (1, 2, 3), (7, 4, 1), (8, 5, 2), (9, 6, 3), (7, 5, 3),
                 (1, 5, 9))

        for i, l in enumerate(lines):
            ttt[i] = (bo[l[0]] == letter and bo[l[1]] == letter and bo[l[2]] == letter)
            maxind[i] = max(nb[l[0]], nb[l[1]], nb[l[2]])
            if ttt[i]:
                has_ttt = True

        if has_ttt:
            lowest_max_subscript = min([ind for ind, line in zip(maxind, ttt) if line])

        return has_ttt, lowest_max_subscript

    def isFull(self):
        """ Return True if every space on the board has been taken. Otherwise return False. """
        for i in range(1, 10):
            if self.isSpaceFree(i):
                return False
        return True

    def checkAlmostFull(self):
        """ Checks if exactly one square is free, in which case fills it with the letter which
        didn't start the game. """
        free = False
        for i in range(1, 10):
            if self.isSpaceFree(i):
                if free:
                    return
                free = i
        if free:
            letter = "X" if self.startingLetter is "O" else "X"
            self.addFinMark(letter, self.lastMark.num+1, free)

    def realBoard(self):
        """ Returns only the "real" board, i.e. measured fields """
        bo = [' '] * 10
        num = [' '] * 10
        for f in self.fields:
            for m in f.contents:
                if isinstance(m, FinMark):
                    bo[f.num] = m.letter
                    num[f.num] = m.num
        return bo, num

    def __str__(self):
        """ A crude representation of the board on screen """
        w = 16  # Field width (in spaces)
        fs = map(lambda f: f.fieldToString() if isinstance(f, Field) else None, self.fields)
        s = fs[7] + " " * (w - len(fs[7])) + "|" + fs[8] + " " * (w - len(fs[8])) + "|" + fs[9]
        s += "\n" + "-" * (3 * w + 4) + "\n"
        s += fs[4] + " " * (w - len(fs[4])) + "|" + fs[5] + " " * (w - len(fs[5])) + "|" + fs[6]
        s += "\n" + "-" * (3 * w + 4) + "\n"
        s += fs[1] + " " * (w - len(fs[1])) + "|" + fs[2] + " " * (w - len(fs[2])) + "|" + fs[3]

        return s

    def isSpaceFree(self, move):
        """ Return true if the passed move is free on the passed board. """
        board, _ = self.realBoard()
        return 1 <= move <= 9 and board[move] == ' '

    def makeMove(self, movecode):
        """ compact way of making all possible varieties of moves:
        1) only a normal move
        2) a collapse and a normal move
        3) only a collapse, after which the game ends
        coding works as follows: movecode = [collapse_pars, move_pars], where
        collapse_pars = "X328" means X3 collapses at position 2, not at position 8
        collapse_pars = "" means: no collapse possible
        move_pars = "O321" means: O3 at positions 2 and 1
        move_pars = "" means: after the collapse, there is no move possible as the match ends
        both shouldn't be empty at the same time
        """
        c_p = movecode[0]
        m_p = movecode[1]
        if c_p:
            self.collapse(c_p[0], c_p[1], c_p[2], c_p[3])
            self.checkAlmostFull()
        if m_p:
            self.addPreMark(m_p[0], m_p[1], m_p[2], m_p[3])

    def getListOfMovecodes(self):
        # TODO: use other_letter(lastMark.letter)
        """ Returns a list of all possible combinations of collapses and premark settings for the
        player with @letter given the @lastMark
        This is a sophisticated version of getListOfMoves because it tries to find
        :param lastMark: The last PreMark on the board.
        :param letter: The letter of the current player
        :param num: Value of the global move counter for the move to be made (i.e. lastMark.num+1)
        :return: list of [collapse_params, move_params]
        """
        # first find possibility of collapse
        lastMark = self.lastMark
        letter = other_letter(lastMark.letter)
        num = lastMark.num + 1
        collapseNecessary = False
        c_p1 = []
        c_p2 = []
        if lastMark and self.findCycle(lastMark.pos):
            collapseNecessary = True
            c_p1 = [lastMark.letter, lastMark.num, lastMark.pos, lastMark.otherpos]
            c_p2 = [lastMark.letter, lastMark.num, lastMark.otherpos, lastMark.pos]
        copyboard1 = self.copy()
        copyboard2 = self.copy()
        gameEnds1 = False
        gameEnds2 = False
        if collapseNecessary:
            copyboard1.makeMove([c_p1, []])
            copyboard2.makeMove([c_p2, []])
            if copyboard1.has_won(letter)[0]:             # current player has won
                gameEnds1 = True
            elif copyboard1.has_won(lastMark.letter)[0]:  # other player has won
                gameEnds1 = True
            if copyboard2.has_won(letter)[0]:             # current player has won
                gameEnds2 = True
            elif copyboard2.has_won(lastMark.letter)[0]:  # other player has won
                gameEnds2 = True

        # then try all possible following premark settings
        listFreePos1 = [pm for pm in range(1, 10) if copyboard1.isSpaceFree(pm)]
        listFreePos2 = [pm for pm in range(1, 10) if copyboard2.isSpaceFree(pm)]
        listPossPM1 = []
        listPossPM2 = []

        for m in range(len(listFreePos1)):
            for n in range(m + 1, len(listFreePos1)):
                listPossPM1.append([letter, num, listFreePos1[m], listFreePos1[n]])

        if collapseNecessary:
            for m in range(len(listFreePos2)):
                for n in range(m + 1, len(listFreePos2)):
                    listPossPM2.append([letter, num, listFreePos2[m], listFreePos2[n]])
        lomc = []

        if listPossPM1:
            if gameEnds1:
                lomc.append([c_p1, []])
            else:
                lomc.extend([c_p1, m_p1] for m_p1 in listPossPM1)
            if gameEnds2:
                lomc.append([c_p2, []])
            elif collapseNecessary:  # gameEnds1 != gameEnds2 only if there is a collapse first
                lomc.extend([c_p2, m_p2] for m_p2 in listPossPM2)
        elif collapseNecessary:
            lomc = [[c_p1, []], [c_p2, []]]
        return lomc

    def getListOfMoves(self):
        """ returns all possible moves """
        listOfMoves = []
        for move in range(1, 10):
            if self.isSpaceFree(move):
                listOfMoves.append(move)
        return listOfMoves

    def makeSteps(self, currentFieldNum, initialFieldNum):
        """ Used recursively to find a cycle in the maze of premarks in order to find out whether
         a collapse will take place """
        conts = self.fields[currentFieldNum].contents # all possible marks from the current position
        listOfNext = [c.copy() for c in conts]
        for m in listOfNext:
            if isinstance(m, FinMark):
                continue
            nextNum = m.otherpos
            cboard = self.copy()
            # delete the current mark from the copied board in order
            # not to use this connection as a path later
            cboard.fields[currentFieldNum].deletePreMark_(m.letter, m.num)
            cboard.fields[nextNum].deletePreMark_(m.letter, m.num)
            # in case we found our way back, we return this mark
            if nextNum == initialFieldNum:
                return m
            else:  # if we didn't make it yet, we go one step deeper
                res = cboard.makeSteps(nextNum, initialFieldNum)
                if res:
                    return res
                else:
                    # if "one step deeper" runs into a dead end,
                    # we take the other option in the list above
                    continue

    def findCycle(self, markStartingFrom):
        if not 1 <= markStartingFrom <= 9:
            return None
        copyBoard = self.copy()
        m = copyBoard.makeSteps(markStartingFrom, markStartingFrom)
        return m

    def collapse(self, markletter, marknum, collapseAt, collapseNotAt):
        """ Collapses the entanglement starting with @markletter, @marknum at position @collapseAt,
         which has its second pos as @collapseNotAt """
        currentField = self.fields[collapseAt]
        otherField = self.fields[collapseNotAt]
        currentField.deletePreMark_(markletter, marknum)
        otherField.deletePreMark_(markletter, marknum)
        listOfMarks = list(currentField.contents)
        currentField.addFinMark(FinMark(markletter, marknum, collapseAt))
        for markToBeReplaced in listOfMarks:
            m = markToBeReplaced
            if isinstance(m, FinMark):
                continue
        for markToBeReplaced in listOfMarks:
            m = markToBeReplaced
            if isinstance(m, FinMark):
                continue
            self.collapse(m.letter, m.num, m.otherpos, m.pos)

    def value(self, player, other_player):
        wincond = [self.has_won(player), self.has_won(other_player)]
        if wincond[0][0]:  # if player 1 has Tic Tac Toe
            if wincond[1][0]:  # and player 2 has Tic Tac Toe
                if wincond[0][1] < wincond[1][1]:  # player 1 actually won
                    return 1
                else:
                    return -1
            else:  # only player 1 has TTT
                return 1
        elif wincond[1][0]:
            return -1
        else:
            return 0



# the following classes Field, PreMark and FinMark are boring but contain a more
# compact description of the atomic objects Field, PreMark, FinMark in the game
class Field:
    def __init__(self, num):
        self.contents = []
        self.num = num

    def copy(self):
        f = Field(self.num)
        for m in self.contents:
            f.contents.append(m.copy())
        return f

    def addPreMark(self, pmark):
        if self.num != pmark.pos:
            print("Error: Wrong Mark position")
            return
        self.contents.append(pmark)

    def addFinMark(self, fmark):
        if self.num != fmark.pos:
            print("Error: Wrong Final Mark position")
            return
        self.contents.append(fmark)

    def deletePreMark(self, pmark):
        try:
            self.contents.remove(pmark)
        except ValueError:
            print("Error: PreMark not in Field")

    def deletePreMark_(self, letter, num):
        for m in self.contents:
            if isinstance(m, PreMark) and m.num == num and m.letter == letter:
                self.contents.remove(m)

    def deleteFinMark(self, fmark):
        try:
            self.contents.remove(fmark)
        except ValueError:
            print("Error: Final Mark not in Field")

    def fieldToString(self):
        for m in self.contents:
            if isinstance(m, FinMark):
                return m.letter
        # so apparently there is no final mark
        s = ""
        for m in self.contents:
            s += m.letter + str(m.num) + ", "
        return s


class PreMark:
    def __init__(self, letter, num, pos, otherpos):
        self.letter = letter
        self.num = num
        self.pos = pos
        self.otherpos = otherpos

    def __str__(self):
        return "PreMark: {}{}{}{}".format(self.letter, self.num, self.pos, self.otherpos)

    def copy(self):
        return PreMark(self.letter, self.num, self.pos, self.otherpos)


class FinMark:
    def __init__(self, letter, num, pos):
        self.letter = letter
        self.pos = pos
        self.num = num

    def __str__(self):
        return "FinMark: {}{}{}".format(self.letter, self.num, self.pos)

    def copy(self):
        return FinMark(self.letter, self.num, self.pos)


def other_letter(letter):
    return 'X' if letter == 'O' else 'O'
