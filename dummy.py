import random


def random_move(board):
    lomc = board.getListOfMovecodes()
    return random.choice(lomc) if lomc else None


def random_collapse(board):
    lomc = filter(lambda mc: mc[0] != [], board.getListOfMovecodes())
    return random.choice(lomc) if lomc else None

