from __future__ import print_function
from qttt import *
import random
from players import HumanPlayer, IdioticPlayer, MonteCarloPlayer, MinimaxPlayer
import curses
from tools import cdisplay
from time import sleep
import argparse


def play_again(win):
    """ Returns True iff the player wants to play again. """
    curses.echo()
    again = "x"
    while again not in "yn":
        cdisplay(win, "Play again? [Y/n]: ")
        again = win.getstr().lower()
    curses.noecho()
    return again != "n"


def new_game(players, turn='random', status_win=None, board_win=None):
    turn = {'random': random.randint(0, 1), 'player1': 0, 'player2': 1}[turn]
    theBoard = Board(startingLetter=players[turn].letter)
    new_game.winner = None  # Use function attribute since we don't have Python 3's "nonlocal" kwd.

    def game_has_ended():
        # look at winning conditions:
        p1won, p1lms = theBoard.has_won(players[0].letter)
        p2won, p2lms = theBoard.has_won(players[1].letter)
        new_game.winner = None
        if p1won:
            new_game.winner = 0
            if p2won:  # Decide who wins using the lowest max subscript
                new_game.winner = 0 if p1lms < p2lms else 1
        elif p2won:
            new_game.winner = 1
        if new_game.winner is not None:
            cdisplay(status_win, "{} has won the game!".format(players[new_game.winner]))
            return True
        elif theBoard.isFull():
            cdisplay(status_win, "The game is a tie!")
            return True
        return False

    while True:  # loop for turns
        player = players[turn]
        cdisplay(status_win, "It's {0}'s turn".format(player))
        cdisplay(board_win, str(theBoard))
        if theBoard.findCycle(theBoard.lastMark.pos):  # Is there entanglement after last move?
            collapse = player.get_collapse(theBoard)
            theBoard.makeMove([collapse, []])
            cdisplay(board_win, str(theBoard))
        if game_has_ended():
            break
        move = player.get_move(theBoard)
        theBoard.makeMove([[], move])
        turn = (turn + 1) % 2

    return new_game.winner

    
def interactive_match(stdscr, args):
    curses.curs_set(0)
    frame = curses.newwin(7, 55, 0, 0)
    frame.box()
    frame.refresh()
    board_win = curses.newwin(5, 53, 1, 1)
    results_win = curses.newwin(1, 70, 7, 3)
    status_win = curses.newwin(1, 100, 8, 1)
    debug_win = curses.newwin(3, 100, 11, 1) if args.stats else None
    wins = [0, 0]
    ngames = 0
    switch = {'human': HumanPlayer, 'idiot': IdioticPlayer,
              'minimax': MinimaxPlayer, 'montecarlo': MonteCarloPlayer}
    players = [None, None]
    while True:
        players[0] = switch[args.playerX](letter="X",
                                          status_win=status_win, debug_win=debug_win,
                                          opts=args.opts)
        players[1] = switch[args.playerO](letter="O",
                                          status_win=status_win, debug_win=debug_win,
                                          opts=args.opts)
        winner = None
        ngames += 1
        try:  # FIXME: HACK until we fix displaying of more than 4 PreMarks (CAREFUL!)
            if debug_win:
                debug_win.erase()
                debug_win.refresh()
            winner = new_game(players, turn='random', status_win=status_win, board_win=board_win)
        except curses.error:
             pass
        if winner is not None:
            wins[winner] += 1
        sleep(1)
        cdisplay(results_win, "{0}: {2}  ---  {1}: {3}  ---  Draws: {4}".
                 format(players[0].name, players[1].name, wins[0], wins[1], ngames - sum(wins)))
        if not play_again(status_win):
            return

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-s", "--stats", help="Display some statistics", action='store_true')
    parser.add_argument("-x", "--playerX", help="Player 1 (X)",
                        choices=['human', 'idiot', 'minimax', 'montecarlo'], default='human')
    parser.add_argument("-o", "--playerO", help="Player 1 (O)",
                        choices=['human', 'idiot', 'minimax', 'montecarlo'], default='montecarlo')
    parser.add_argument("-t", "--time", help="Time available for Monte Carlo simulations (seconds)",
                        type=float, default=2.5)
    parser.add_argument("-u", "--uct", help="UCT factor for Monte Carlo player",
                        type=float, default=1.0)
    parser.add_argument("-r", "--recursions", help="Number of recursions for Minimax algorithm",
                        type=int, default=3)
    args = parser.parse_args()
    args.opts = {'time_available': args.time, 'recursions': args.recursions, 'uct_factor': args.uct}
    curses.wrapper(interactive_match, args)

    # b = Board(startingLetter='O')
    # opts = {'time_available': 2.0, 'recursions': 2, 'uct_factor': 2}
    # p1 = MonteCarloPlayer(letter='X', name="Hal", opts=opts)
    # p2 = IdioticPlayer(letter='O', name="Idiot", opts=opts)
    # p3 = MonteCarloPlayer(letter='X', name="Cheater", opts=opts)
    # b.makeMove([[], ['X', 1, 6, 9]])
    # b.makeMove([[], ['O', 2, 4, 5]])
    # b.makeMove([[], ['X', 3, 7, 8]])
    # b.makeMove([[], ['O', 4, 8, 9]])
    # b.makeMove([[], ['X', 5, 5, 9]])
    # b.makeMove([[], ['O', 6, 3, 9]])
    # b.makeMove([[], ['X', 7, 3, 6]])
