from mcts import mcts, Tree
from minimax import minimax
from qttt import other_letter
from tools import cdisplay
from time import time
import dummy
import random
import curses


class Player(object):
    def __init__(self, letter, name="Player"):
        self.name = name
        self.letter = letter

    def __str__(self):
        return self.name

    def get_collapse(self, board):
        return None

    def get_move(self, board):
        return None


class MonteCarloPlayer(Player):
    def __init__(self, letter, name="The Gambler", status_win=None, debug_win=None, opts=None):
        super(MonteCarloPlayer, self).__init__(letter, name)
        self.has_collapsed = False  # True if get_collapse() was called before get_move()
        self.cached_move = None
        self.stored_tree = Tree()
        self.time_available = opts['time_available']
        self.uct_factor = opts['uct_factor']
        self.debug_win = debug_win

    def next_move(self, board):
        """ Driver of the Monte Carlo Tree Search. Returns a movecode. """
        best_child = mcts(board, self.stored_tree, self.time_available, self.uct_factor,
                          self.debug_win)
        self.stored_tree = best_child
        return best_child.move

    def get_collapse(self, board):
        self.cached_move = self.next_move(board)
        self.has_collapsed = True
        return self.cached_move[0]

    def get_move(self, board):
        if not self.has_collapsed:
            self.cached_move = self.next_move(board)
        self.has_collapsed = False
        return self.cached_move[1]


class IdioticPlayer(Player):
    def __init__(self, letter, name="The Idiot", status_win=None, debug_win=None, opts=None):
        super(IdioticPlayer, self).__init__(letter, name)

    def get_collapse(self, board):
        return dummy.random_collapse(board)[0]

    def get_move(self, board):
        return dummy.random_move(board)[1]


class HumanPlayer(Player):
    def __init__(self, letter, name="The Human", status_win=None, debug_win=None, opts=None):
        super(HumanPlayer, self).__init__(letter, name)
        self.status_win = status_win  # curses window for interactions

    def get_collapse(self, board):
        """ Let the player type in their desired collapse target. """
        lastMark = board.lastMark
        cdisplay(self.status_win, "You may collapse {0}{1} on field {2} or {3}: [{2}/{3}]".
                 format(lastMark.letter, lastMark.num, lastMark.pos, lastMark.otherpos))
        curses.echo()

        def read_number():
            try:
                return int(self.status_win.getkey())
            except ValueError:
                return -1

        choice = read_number()
        while choice not in [lastMark.pos, lastMark.otherpos]:
            cdisplay(self.status_win, "Choose {0} or {1}: ".format(lastMark.pos, lastMark.otherpos))
            choice = read_number()
        curses.noecho()
        other = lastMark.otherpos if choice == lastMark.pos else lastMark.pos
        return board.lastMark.letter, board.lastMark.num, choice, other

    def get_move(self, board):
        """ Let the player type in their move. """
        curses.echo()
        move1 = move2 = None
        while move1 == move2 or not board.isSpaceFree(move1) or not board.isSpaceFree(move2):
            cdisplay(self.status_win, "What is your move? (Two digits 1-9): ")
            l = self.status_win.getstr()
            if len(l) == 2:
                move1 = int(l[0])
                move2 = int(l[1])
        curses.noecho()
        return self.letter, board.lastMark.num + 1, move1, move2


class MinimaxPlayer(Player):
    def __init__(self, letter, name="The Obtuse", status_win=None, debug_win=None, opts=None):
        super(MinimaxPlayer, self).__init__(letter, name)
        self.has_collapsed = False  # True if get_collapse() was called before get_move()
        self.cached_move = None
        self.recursions = opts['recursions']
        self.debug_win = debug_win

    def next_move(self, board):
        """ Driver of the minimax algorithm. Returns a movecode. """
        other = board.lastMark.letter
        letter = other_letter(other)
        num = board.lastMark.num + 1
        start = time()
        _, moves = minimax(board, self.recursions, self.recursions, True, letter, num, other)
        cdisplay(self.debug_win, "Minimax took {0:.3}s to do {1} recursions"
                 .format(time()-start, self.recursions))
        return random.choice(moves)

    def get_collapse(self, board):
        self.cached_move = self.next_move(board)
        self.has_collapsed = True
        return self.cached_move[0]

    def get_move(self, board):
        if not self.has_collapsed:
            self.cached_move = self.next_move(board)
        self.has_collapsed = False
        return self.cached_move[1]

