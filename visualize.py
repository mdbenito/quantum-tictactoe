import argparse
import pickle
import numpy as np
import matplotlib.pyplot as plt
from benchmark import Experiment

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("input", help="Name of file with pickled results", type=str, default="results.pickle")
parser.add_argument("-f", "--factor", help="Multiplicative scale factor for the blobs", type=float, default=2.0)
parser.add_argument("-p", "--power", help="Exponentiation scale factor for the blobs", type=float, default=2.0)
parser.add_argument("-m", "--markerscale", help="Scale of marker in the legend", type=float, default=0.5)
parser.add_argument("-o", "--output", help="Base name of output plots (png and eps)", default="qtt-benchmark")
args = parser.parse_args()

exp = None
with open(args.input, 'rb') as f:
    exp = pickle.load(f)
r1 = exp.wins_p1.astype(np.float64)
r2 = exp.wins_p2.astype(np.float64)
uct_values = exp.uct_values.astype(np.float64)
ngames = exp.games_per_match
n = len(uct_values)

# Because of symmetry, results are only computed for the lower triangle of the
# parameter matrix, so we must merge them now: The number of wins of P1 for
# parameters [i, j] (j>1) is equal to the number of wins of P2 for parameters [j, i]
# and viceversa.

tmp = r2.T.copy()
r2 += r1.T
r1 += tmp
r1[np.diag_indices(n)] /= 2.
r2[np.diag_indices(n)] /= 2.

games = ngames * np.ones_like(r1)
draws = games - (r1+r2)

# Compute winner masks for the plots
p1wins = r1 > r2
p2wins = r1 < r2
nowins = r1 == r2

# Values of the parameter for player 1 go on axis 1, for player 2 on axis 2
xx, yy = np.meshgrid(uct_values, uct_values)
x = xx.flatten()
y = yy.flatten()

# Size for the blobs in the scatterplot:
# The larger the difference in the number of victories is, the larger the blob.
# When both players won the same amount of games, plot only the number of draws.
sizes = np.abs(r1-r2)
sizes[nowins] = draws[nowins]
sizes = (args.factor*sizes) ** args.power

colors = np.ones_like(r1)  # blue: same number of wins (size represents number of draws)
colors[p1wins] = 2  # red: player1 won more times than player 2
colors[p2wins] = 3  # green: player2 won more times than player 1
colors = map(lambda c: 'b' if c == 1 else 'r' if c == 2 else 'g', np.nditer(colors, order='C'))

#plt.scatter(x, y, c=colors, s=sizes.flatten(), alpha=0.5)

plt.scatter(xx[p1wins].flatten(), yy[p1wins].flatten(), c='r', s=sizes[p1wins].flatten(), alpha=0.5, label='P1')
plt.scatter(xx[p2wins].flatten(), yy[p2wins].flatten(), c='g', s=sizes[p2wins].flatten(), alpha=0.5, label='P2')
# Plot draws only if both players won the same amount of games...
#plt.scatter(xx[nowins].flatten(), yy[nowins].flatten(), c='b', s=sizes[nowins].flatten(), alpha=0.5, label='Draw')
# Or plot them always:
plt.scatter(xx.flatten(), yy.flatten(), c='#B2DFEE', s=(args.factor*draws.flatten())**args.power, alpha=0.3, label='Draw')

plt.legend(scatterpoints=1, markerscale=args.markerscale)
plt.axes().set_aspect('equal')
plt.savefig(args.output + '.eps')
plt.savefig(args.output + '.png')

plt.show()
