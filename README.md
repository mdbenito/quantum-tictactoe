# Monte Carlo Quantum Tic-Tac-Toe

This is an implementation of [Quantum Tic Tac Toe](https://en.wikipedia.org/wiki/Quantum_tic-tac-toe)
with several AIs to choose from, in particular
[Monte Carlo Tree Search](https://en.wikipedia.org/wiki/Monte_Carlo_tree_search).
It is heavily based on [Philipp Wacker's qTTT](https://github.com/PhilippWacker/quantum-tictactoe),
itself based on [Al Sweigart's sample implementation](https://inventwithpython.com/chapter10.html)
of classical Tic Tac Toe.

Our main contributions are:

* An implementation of Monte Carlo tree search. 
* Game modes: player vs player, computer vs computer.
* Benchmarking of MCTS and visualization of results.
* Better game interface using ncurses (still very primitive).
* Code refactoring and cleanup.
* Bigger board sizes. *(TODO)*


## Dependencies

* Python >= 2.6 (I guess)
* The curses library (which probably means that the interface will only run on Linux / OSX / Unices)

For the benchmarking of MCTS:

* [futures](https://pypi.python.org/pypi/futures), a backport of concurrent.futures to Python 2
* Numpy

The easiest is to install everything using [conda](http://conda.pydata.org/). If you are not using
it you should!

## Playing

Just run

``python play.py``

in your console to play against the computer. Use

``python play.py --help``

for command line options.

## Game AIs and bechmarking

Currently, three game AIs are implemented: Random, Minimax and MCTS.

### Random

Not very bright as an AI. Will just pick a move at random from the list of possible ones.

### Minimax

[Minimax](https://en.wikipedia.org/wiki/Minimax) tries to build the entire game tree and compute
scores for each outcome, maximising the score of the computer and minimising the score of the
player. For QTTT this is too costly: the tree is at least of size ~10^14, according to [3], which
for an unrealistically minimal node size of 10 bytes implies _1 Petabyte_ of storage if one were
to store the whole tree. Given this size it is imperative to fix an arbitrary (an small!) tree depth
to stop the minimax recursions, thus yielding wholly inaccurate estimates of the scores for most of
the time.

### Monte Carlo tree search

See [Wikipedia](https://en.wikipedia.org/wiki/Monte_Carlo_tree_search) for a short intro to MCTS.

We do a basic implementation using UCT (Upper Confidence Bound for Trees, [4]) with a tunable
parameter balancing exploration of new venues and exploitation of choices known to be good.
It is possible to run benchmarks matching each value in a list against all others.
See `benchmark.py`. Edit and run `python visualize.py` to plot the results.

We introduced a slight modification of the standard rewards -1, 0, +1, by accounting for the length
of the game leading to the result. In order to make the game more interesting for a human player
(and leave more room for mistakes) we favor shorter paths for winning games, while for losing games
we prefer longer paths (see `backpropagate()` in `mcts.py`).

## A note about the game

In [3] the authors compute optimal strategies under the criterion of "complete win" (only one player
has TicTacToe) and "narrow win" (both players have TicTacToe, with one having collapsed its marks
before). In the first case, the game turns out to be a draw and in the second a victory for the
first player in exactly nine moves for an initial placement of marks at two opposite corners of the
board (e.g. (1, 9)).

## Authors

* Miguel de Benito Delgado: m DOT debenito DOT d (gmail)
* Ana Cañizares García: a DOT canizares DOT garcia (gmail)
* Philipp Wacker: phwacker (gmail)

## License

This software falls under the GNU general public license version 3 or later.
It comes without **any warranty whatsoever**.
For details see http://www.gnu.org/licenses/gpl-3.0.html.


## References

[1] C. B. Browne, E. Powley, D. Whitehouse, S. M. Lucas, P. I. Cowling, P. Rohlfshagen, S. Tavener,
    D. Perez, S. Samothrakis, and S. Colton, “A Survey of Monte Carlo Tree Search Methods,”
    IEEE Transactions on Computational Intelligence and AI in Games, vol. 4, no. 1, pp. 1–43,
    Mar. 2012.

[2] A. Goff, “Quantum tic-tac-toe: A teaching metaphor for superposition in quantum mechanics,”
    American Journal of Physics, vol. 74, no. 11, pp. 962–973, Nov. 2006.

[3] T. Ishizeki and A. Matsuura, “Solving Quantum Tic-Tac-Toe,” presented at the International
    Conference on Advanced Computing Technologies, India, 2011.
    
[4] L. Kocsis and C. Szepesvári, “Bandit Based Monte-carlo Planning,” in Proceedings of the 17th
    European Conference on Machine Learning, Berlin, Heidelberg, 2006, pp. 282–293.